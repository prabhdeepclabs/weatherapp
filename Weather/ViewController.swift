//
//  ViewController.swift
//  Weather
//
//  Created by Bhasker on 1/21/15.
//  Copyright (c) 2015 Bhasker. All rights reserved.
//

import UIKit

var urlString = "" // url string as global for use in both view controllers

class ViewController: UIViewController {

    @IBOutlet weak var details: UIButton! // Button for more details in new view controller
    
    @IBOutlet weak var enteredCity: UITextField! // entered city name in Text field
    
    @IBOutlet weak var weatherReport: UILabel! // display weather report through Label
    
    @IBAction func findWeather(sender: AnyObject) {
        // fetch the weather report from a website for the entered city name and display it through label
        self.view.endEditing(true)
        
        // create url for that city
        urlString = "http://www.weather-forecast.com/locations/" + enteredCity.text.stringByReplacingOccurrencesOfString(" ", withString: "") + "/forecasts/latest"
        var url = NSURL(string: urlString)
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) { (data, response, error) in
            // task to fetch content i.e. weather report from url
            var urlContent = NSString (data: data, encoding: NSUTF8StringEncoding)
            
            if (urlContent!.containsString("<span class=\"phrase\">")) {
                // if valid city name then only look for required content
                
                var contentArray = urlContent?.componentsSeparatedByString("<span class=\"phrase\">")
                var newContentArray = contentArray?[1].componentsSeparatedByString("</span>")
                
                // required content is in array index 0
                println( newContentArray![0].stringByReplacingOccurrencesOfString("&deg;", withString: "º") )
                
                dispatch_async(dispatch_get_main_queue()) {
                    self.weatherReport.text = newContentArray![0].stringByReplacingOccurrencesOfString("&deg;", withString: "º") as String
                }
                
            } else {
                // if valid city name not entered
                dispatch_async(dispatch_get_main_queue()) {
                    self.weatherReport.text = "Cannot find city - try again "
                }
            }
        }
        task.resume()
        details.hidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        details.hidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

