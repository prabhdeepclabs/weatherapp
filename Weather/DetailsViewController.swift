//
//  DetailsViewController.swift
//  Weather
//
//  Created by Bhasker on 1/22/15.
//  Copyright (c) 2015 Bhasker. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // loading web view
        let url = NSURL(string: (urlString + "/threedayfree"))
        let request = NSURLRequest (URL: url!)
        webView.loadRequest(request)
    }
    
    // for interface orientation
    override func supportedInterfaceOrientations() -> Int {
        return Int(UIInterfaceOrientationMask.Landscape.rawValue)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
